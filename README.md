# sauce
An extremely rickety extension of `source ~/.zshrc`

I use these bash commands to reload yarn and rbenv when switching between projects. 

To get started, run `nsh core` to edit `core.zsh` with `nano`.
Then, define a function that takes you to a project directory. 
```
project_name () {
    ~/repos/rails/project_name/
}
```

running the command, then `sauce` in the destination folder will: 
- set node version
- set ruby version
- regenerate symlinks between project dotenv files and env files stored locally
- regenerate symlinks on ssh config files (commented out for now)

There are many other useful shorthand commands within `zsh/core.d`, which mostly deal with common Rails tools - sidekiq, postgres, redis etc. 

# TO DO: 
- add env files to gitignore
- address symlink issue with docker-compose commands
